import numpy as np

from Canopy3D import Canopy3D

def CanopyProperties(contourC, nVeg, vegHeight, vegetation, maxHeight, nx, ny, dx):

    vegGrid3D = np.ones([nx + 1, ny + 1, maxHeight.astype(int) + 1])

    for i in np.arange(0, nVeg):
        #print(contourC.collections[0].get_paths()[i].vertices)
        CanopyRegion = Canopy3D(i, nx, ny, contourC.collections[0].get_paths(), maxHeight)
        zi = 0.5 * (np.mean(vegHeight[CanopyRegion.coords[:, 0], CanopyRegion.coords[:, 1]]) + np.max(vegHeight[CanopyRegion.coords[:, 0], CanopyRegion.coords[:, 1]]))
        zv = np.ceil(zi).astype(int)
        vegGrid3D[CanopyRegion.coords[:, 0], CanopyRegion.coords[:, 1], 0:zv] = 2.


    GroundI = np.where(vegetation != 2.)
    vegGrid3D[GroundI[0], GroundI[1], :] = 1.


    return contourC, vegGrid3D
