import numpy as np

from Buildings3D import Buildings3D

def BuildingProperties(contourL, nBuilds, walls, builds, nx, ny, dx):

    ##### Create walls of each building and number of them
    linesF = []
    for line in contourL.collections[0].get_paths():
        linesF.append(np.floor(np.fliplr(line.vertices)).astype(int))

    linesC = []
    for line in contourL.collections[0].get_paths():
        linesC.append(np.ceil(np.fliplr(line.vertices)).astype(int))

    #################
    ### AvgHeight of buildings, respectively
    heightsF = []
    for i in np.arange(0, nBuilds):
        height = walls[linesF[i][:, 0], linesF[i][:, 1]]
        heightsF.append(np.nanmean(np.nanmean(np.where(height != 0., height, np.nan), 0)))

    heightsC = []
    for i in np.arange(0, nBuilds):
        height = walls[linesC[i][:, 0], linesC[i][:, 1]]
        heightsC.append(np.nanmean(np.nanmean(np.where(height != 0., height, np.nan), 0)))

    heights = np.nanmean(np.column_stack((heightsF, heightsC)), axis=1)

    maxHeight = np.round(np.nanmax(heights) + 10 * dx) ## 10 levels more above highest building

    ##################
    ### Create 3D building grid and properties of each building
    buildGrid3D = np.ones([nx + 1, ny + 1, maxHeight.astype(int) + 1])
    buildBBox = []
    buildCenter = []
    buildOrient = []
    buildMajorL = []
    buildMinorL = []
    buildInnerReg = []
    for i in np.arange(0, nBuilds):
        BuildRegion, zi = Buildings3D(i, nx, ny, contourL.collections[0].get_paths(), heights, maxHeight)
        buildInnerReg.append(BuildRegion.coords)
        buildBBox.append(BuildRegion.bbox)
        buildCenter.append(BuildRegion.centroid)
        buildOrient.append(BuildRegion.orientation)
        buildMajorL.append(BuildRegion.major_axis_length * dx)
        buildMinorL.append(BuildRegion.minor_axis_length * dx)
        buildGrid3D[BuildRegion.coords[:, 0], BuildRegion.coords[:, 1], 0:zi] = 0.

    GroundI = np.where(builds != 0.)
    buildGrid3D[GroundI[0], GroundI[1], :] = 1.


    return buildInnerReg, contourL, buildGrid3D, buildBBox, buildCenter, buildOrient, buildMajorL, buildMinorL, heights, maxHeight, linesC, linesF
