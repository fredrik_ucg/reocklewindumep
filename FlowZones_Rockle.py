
import numpy as np

import matplotlib.pyplot as plt
import operator
from scipy.spatial import distance
from math import radians, degrees

### inner regions ###
from FrontZone_Index import FrontZone
from BackZone_Index import BackZone
from BuildingProperties import BuildingProperties
from CanopyProperties import CanopyProperties
from analyse_buildings import NN_dirwalls, list_of_dirwalls, cornerPoints, cornerPoints2


def deleteSmallObjects(contour,smallOb):
    for i in np.arange(0, len(contour.collections[0].get_paths())):
        if len(contour.collections[0].get_paths()[i]) < 18:
            smallOb.append(i)

    for AY in sorted(smallOb, key=lambda AY: -AY):
        contour.collections[0].get_paths().pop(AY)

    return contour


#@profile
def FlowZonesR(builds, dirwalls, walls, ff, dd, vegheight, vegetation, densityplants, zref, nx, ny, dx, dy):
    rad = np.pi / 180.

    # This is the function calculating the flow regimes, respectively
    # 2017-Nov-16
    # Sandro Oswald, sandro.oswald@boku.ac.at
    # Vienna Urban Climate Group
    # University of natural sciences (BOKU)

    CV = plt.contour(builds, 1, linewidths=.5)
    CC = plt.contour(vegetation, 1, linewidths=.5)
    smB = []
    smC = []

    Cbuild = deleteSmallObjects(CV, smB)
    Ccanopy = deleteSmallObjects(CC, smC)

    nBuilds = len(Cbuild.collections[0].get_paths())
    nVeg = len(Ccanopy.collections[0].get_paths())

    _, contourL, buildGrid3D, buildBBox, _, _, _, _, _, \
    _, _, _ = BuildingProperties(CV, nBuilds, walls, builds, nx, ny, dx)


    indexInside = []
    courtyards = []
    for i in np.arange(0, len(buildBBox)):
        for j in np.arange(0, len(buildBBox)):
            if (buildBBox[i][0] > buildBBox[j][0]) and (buildBBox[i][1] > buildBBox[j][1]) and (buildBBox[i][2] < buildBBox[j][2]) and (buildBBox[i][3] < buildBBox[j][3]):
                indexInside.append(i)
                courtyards.append(contourL.collections[0].get_paths()[i]) ### Save courtyards indices
            else:
                continue

    indexInside = list(set(indexInside))

    for level in contourL.collections:
        for kp, path in reversed(list(enumerate(level.get_paths()))):
            for i in np.arange(0, len(indexInside)):
                if kp == indexInside[i]:
                    del (level.get_paths()[kp])


    nBuilds = len(contourL.collections[0].get_paths())

    Buildregion, contourM, _, buildBBox, buildCenter, buildOrient, buildMajorL, buildMinorL, heights, \
    maxHeight, linesC, linesF = BuildingProperties(contourL, nBuilds, walls, builds, nx, ny, dx)

    contourC, vegGrid3D = CanopyProperties(CC, nVeg, vegheight, vegetation, maxHeight, nx, ny, dx)

    ##################
    ### Walls in direction
    wallN = np.where(np.logical_and(np.greater_equal(dirwalls, 315.), np.not_equal(dirwalls, 0.)))
    wallN2 = np.where(np.logical_and(np.less_equal(dirwalls, 45.), np.not_equal(dirwalls, 0.)))
    wallNe = np.column_stack((np.stack(wallN)[0], np.stack(wallN)[1]))
    wallNe2 = np.column_stack((np.stack(wallN2)[0], np.stack(wallN2)[1]))
    wallNa = np.concatenate((wallNe, wallNe2), axis=0)

    wallS = np.where(np.logical_and(np.greater_equal(dirwalls, 135.), np.less_equal(dirwalls, 225.)))
    wallSe = np.column_stack((np.stack(wallS)[0], np.stack(wallS)[1]))

    wallE = np.where(np.logical_and(np.greater(dirwalls, 45.), np.less(dirwalls, 135.)))
    wallEe = np.column_stack((np.stack(wallE)[0], np.stack(wallE)[1]))

    wallW = np.where(np.logical_and(np.greater(dirwalls, 225.), np.less(dirwalls, 315.)))
    wallWe = np.column_stack((np.stack(wallW)[0], np.stack(wallW)[1]))

    listN = []
    listE = []
    listS = []
    listW = []
    listNW = []
    listEW = []
    listSW = []
    listWW = []

    listE, listEW = list_of_dirwalls(linesC, wallEe, listE, listEW, dirwalls)
    listS, listSW = list_of_dirwalls(linesF, wallSe, listS, listSW, dirwalls)
    listW, listWW = list_of_dirwalls(linesF, wallWe, listW, listWW, dirwalls)
    listN, listNW = list_of_dirwalls(linesC, wallNa, listN, listNW, dirwalls)


    #### Calc z zero and average height #########
    nz = np.size(buildGrid3D, 2)
    z = np.linspace(0, nz-1, nz)

    lengthBi = np.zeros(nBuilds)
    widthBi = np.zeros(nBuilds)

    #### Calc length and witdth of buildings regarding the wind direction ####
    if (dd > 45.) and (dd < 135.) or (dd > 225.) and (dd < 315.):
        for i in np.arange(0, nBuilds):
            if degrees(buildOrient[i]) < -45.:
                lengthBi[i] = np.array(buildMinorL[i])
                widthBi[i] = np.array(buildMajorL[i])
            elif degrees(buildOrient[i]) > 45.:
                lengthBi[i] = np.array(buildMinorL[i])
                widthBi[i] = np.array(buildMajorL[i])
            else:
                lengthBi[i] = np.array(buildMajorL[i])
                widthBi[i] = np.array(buildMinorL[i])

            if lengthBi[i] == 0.:
                lengthBi[i] = 1.
            if widthBi[i] == 0.:
                widthBi[i] = 1.
    else:
        for i in np.arange(0, nBuilds):
            if degrees(buildOrient[i]) < -45.:
                lengthBi[i] = np.array(buildMajorL[i])
                widthBi[i] = np.array(buildMinorL[i])
            elif degrees(buildOrient[i]) > 45.:
                lengthBi[i] = np.array(buildMajorL[i])
                widthBi[i] = np.array(buildMinorL[i])
            else:
                lengthBi[i] = np.array(buildMinorL[i])
                widthBi[i] = np.array(buildMajorL[i])

            if lengthBi[i] == 0.:
                lengthBi[i] = 1.
            if widthBi[i] == 0.:
                widthBi[i] = 1.


    HeiAvg = np.sum(lengthBi * widthBi * heights) / np.sum(lengthBi * widthBi)

    if zref < HeiAvg:
        zref = HeiAvg

    zZero = 0.2 * np.sum(lengthBi * widthBi * heights) / (nx * dx * ny * dy)

    ### Displacement height
    #D = 0.8 * np.sum(lengthBi * widthBi * heights) / np.sum(lengthBi * widthBi)

    uZ = ff * np.log((z - 0.66 * HeiAvg) / zZero) / np.log((zref - 0.66 * HeiAvg) / zZero)
    #uZ2 = ff * np.log((z - D) / zZero) / np.log((zref - D) / zZero)

    Vconst = np.where(z <= HeiAvg)

    uZ[Vconst] = uZ[HeiAvg.astype(int)]
    #uZ2[Vconst] = uZ2[HeiAvg.astype(int)]

    u0 = -uZ * np.sin(radians(dd))
    v0 = -uZ * np.cos(radians(dd))
    w0 = 0.

    u = np.zeros([nx, ny + 1, nz])  # Preallocating u
    v = np.zeros([nx + 1, ny, nz])  # Preallocating v
    w = np.zeros([nx, ny, nz + 1])

    for i in np.arange(0, np.size(u, 2)):
        u[:, :, i] = u0[i]
        v[:, :, i] = v0[i]

    w[:, :, :] = w0

    buildIndex = np.where(buildGrid3D == 0.)
    canopyIndex = np.where(vegGrid3D == 2.)

    u[buildIndex] = 0.
    v[buildIndex] = 0.
    w[buildIndex] = 0.
    w[canopyIndex] = 0.1

    un = np.copy(u)
    vn = np.copy(v)
    wn = np.copy(w)



    #### Front vorticity zone ###########
    Lf = heights * 2. * (widthBi / heights) / (1 + 0.8 * (widthBi / heights))

    effWi = np.zeros(nBuilds)

    for i in np.arange(0, nBuilds):
        if (dd > 45.) and (dd < 135.) or (dd > 225.) and (dd < 315.):
            if (buildOrient[i] / rad < -45.) or (buildOrient[i] / rad > 45.):
                effWi[i] = np.abs(np.cos((dd - buildOrient[i] / rad) * rad)) * widthBi[i] + \
                           np.abs(np.cos((dd + (90 - buildOrient[i] / rad)) * rad)) * lengthBi[i]
            else:
                effWi[i] = np.abs(np.cos((dd - buildOrient[i] / rad) * rad)) * lengthBi[i] + \
                           np.abs(np.cos((dd + (90 - buildOrient[i] / rad)) * rad)) * widthBi[i]
        else:
            if (buildOrient[i] / rad < -45.) or (buildOrient[i] / rad > 45.):
                effWi[i] = np.abs(np.cos((dd - buildOrient[i] / rad) * rad)) * lengthBi[i] + \
                           np.abs(np.cos((dd + (90 - buildOrient[i] / rad)) * rad)) * widthBi[i]
            else:
                effWi[i] = np.abs(np.cos((dd - buildOrient[i] / rad) * rad)) * widthBi[i] + \
                           np.abs(np.cos((dd + (90 - buildOrient[i] / rad)) * rad)) * lengthBi[i]


    effLi = (lengthBi * widthBi) / effWi

    Lr = heights * 1.8 * (effWi / heights) / ((np.abs(effLi) / heights) ** 0.3 * (1 + 0.24 * (effWi / heights)))

    ## X and Y component
    NN = np.zeros([nBuilds, len(z)])
    FN = np.zeros([nBuilds, len(z)])

    for i in np.arange(0, len(heights)):
        for j in np.arange(0, len(z)):
            if z[j] > heights[i]:
                break
            else:
                NN[i, j] = 1.2 * Lr[i] * np.sqrt(1 - (z[j] / (heights[i])) ** 2)
                FN[i, j] = 3. * Lr[i] * np.sqrt(1 - (z[j] / (heights[i])) ** 2)

    ###################


    ##############
    sMean = np.zeros(nBuilds)
    cMean = np.zeros(nBuilds)
    for i in np.arange(0, len(listNW)):
        sMean[i] = np.mean(np.sin(np.array(listNW[i]) * rad))
        cMean[i] = np.mean(np.cos(np.array(listNW[i]) * rad))
        listEW[i] = np.mean(listEW[i])
        listSW[i] = np.mean(listSW[i])
        listWW[i] = np.mean(listWW[i])

    listWW = np.array(np.nan_to_num(listWW))
    listSW = np.array(np.nan_to_num(listSW))
    listEW = np.array(np.nan_to_num(listEW))

    avgN = np.zeros(nBuilds)
    for i in np.arange(0, len(sMean)):
        if cMean[i] < 0.:
            avgN[i] = 180. + np.arctan2(sMean[i], cMean[i]) / rad
        elif (sMean[i] < 0.) and (cMean[i] > 0.):
            avgN[i] = 360. + np.arctan2(sMean[i], cMean[i]) / rad
        else:
            avgN[i] = np.arctan2(sMean[i], cMean[i]) / rad

    listNW = np.nan_to_num(avgN)

    ### Find corner points with minimum distance to BBox
    operator_dict = {
        '+': operator.add,
        '-': operator.sub,
        '*': operator.mul,
        '>': operator.gt,
        '<': operator.lt,
        '>=': operator.ge,
        '<=': operator.le,
        '=': operator.eq
    }

    NPos = []
    WPos = []
    SPos = []
    EPos = []
    NPos2 = []
    WPos2 = []
    SPos2 = []
    EPos2 = []
    NPos3 = []
    WPos3 = []
    SPos3 = []
    EPos3 = []
    NPos4 = []
    WPos4 = []
    SPos4 = []
    EPos4 = []

    NPos = cornerPoints(operator_dict, nBuilds, listN, NPos, buildCenter, listNW, 0, 1, 0, '>=', 180.)
    EPos = cornerPoints(operator_dict, nBuilds, listE, EPos, buildCenter, listEW, 1, 0, 0, '>=', 90.)
    SPos = cornerPoints(operator_dict, nBuilds, listS, SPos, buildCenter, listSW, 0, 1, 1, '>=', 180.)
    WPos = cornerPoints(operator_dict, nBuilds, listW, WPos, buildCenter, listWW, 1, 0, 1, '<=', 270.)

    NPos2 = cornerPoints2(operator_dict, nBuilds, listN, NPos2, buildCenter, listNW, 1, 0, 0, '<=', 180.)
    EPos2 = cornerPoints2(operator_dict, nBuilds, listE, EPos2, buildCenter, listEW, 0, 1, 0, '<=', 90.)
    SPos2 = cornerPoints2(operator_dict, nBuilds, listS, SPos2, buildCenter, listSW, 1, 0, 1, '<=', 180.)
    WPos2 = cornerPoints2(operator_dict, nBuilds, listW, WPos2, buildCenter, listWW, 0, 1, 1, '>=', 270.)

    NPos3 = cornerPoints(operator_dict, nBuilds, listN, NPos3, buildCenter, listNW, 0, 1, 0, '<', 180.)
    EPos3 = cornerPoints(operator_dict, nBuilds, listE, EPos3, buildCenter, listEW, 1, 0, 0, '<', 90.)
    SPos3 = cornerPoints(operator_dict, nBuilds, listS, SPos3, buildCenter, listSW, 0, 1, 1, '<', 180.)
    WPos3 = cornerPoints(operator_dict, nBuilds, listW, WPos3, buildCenter, listWW, 1, 0, 1, '>', 270.)

    NPos4 = cornerPoints2(operator_dict, nBuilds, listN, NPos4, buildCenter, listNW, 1, 0, 0, '>', 180.)
    EPos4 = cornerPoints2(operator_dict, nBuilds, listE, EPos4, buildCenter, listEW, 0, 1, 0, '>', 90.)
    SPos4 = cornerPoints2(operator_dict, nBuilds, listS, SPos4, buildCenter, listSW, 1, 0, 1, '>', 180.)
    WPos4 = cornerPoints2(operator_dict, nBuilds, listW, WPos4, buildCenter, listWW, 0, 1, 1, '<', 270.)

    NPosC = []
    EPosC = []
    SPosC = []
    WPosC = []


    for i in np.arange(0, len(NPos)):
        NPosC.append(np.concatenate((np.array(NPos[i]), np.array(NPos2[i]), np.array(NPos3[i]), np.array(NPos4[i])), axis=0))
        EPosC.append(np.concatenate((np.array(EPos[i]), np.array(EPos2[i]), np.array(EPos3[i]), np.array(EPos4[i])), axis=0))
        SPosC.append(np.concatenate((np.array(SPos[i]), np.array(SPos2[i]), np.array(SPos3[i]), np.array(SPos4[i])), axis=0))
        WPosC.append(np.concatenate((np.array(WPos[i]), np.array(WPos2[i]), np.array(WPos3[i]), np.array(WPos4[i])), axis=0))

    theta = np.linspace(0., 2 * np.pi, 200)
    #thetaS = np.linspace(0., 2 * np.pi, 400)
    thetaN = np.linspace(-np.pi, np.pi, 400)

    print('Front')
    #
    # ################################################
    # ### Calculate front vorticity zones
    #

    if (dd >= 0.) and (dd < 90.):
        windzone = 45.
        y_dir=[0,1,0]
        x_dir=[1,0,0]
        y_wall_list = listNW
        x_wall_list = listEW
        y_pois, y_poisAlt, y_poisAlt2 = [NPosC, WPosC, EPosC]
        x_pois, x_poisAlt, x_poisAlt2 = [NPosC, WPosC, EPosC]

    elif (dd >= 90.) and (dd < 180.):
        windzone = 135.
        y_dir = [0, 1, 2]
        x_dir = [1, 0, 0]
        y_wall_list = listSW
        x_wall_list = listEW
        y_pois, y_poisAlt, y_poisAlt2 = [SPosC, WPosC, EPosC]
        x_pois, x_poisAlt, x_poisAlt2 = [EPosC, SPosC, NPosC]

    elif (dd >= 180.) and (dd < 270.):
        windzone = 225.
        y_dir = [0, 1, 2]
        x_dir = [1, 0, 2]
        y_wall_list = listSW
        x_wall_list = listWW
        y_pois, y_poisAlt, y_poisAlt2 = [SPosC, WPosC, EPosC]
        x_pois, x_poisAlt, x_poisAlt2 = [WPosC, SPosC, NPosC]
    else:
        windzone = 315.
        y_dir = [0, 1, 0]
        x_dir = [1, 0, 2]
        y_wall_list = listNW
        x_wall_list = listWW
        y_pois, y_poisAlt, y_poisAlt2 = [NPosC, WPosC, EPosC]
        x_pois, x_poisAlt, x_poisAlt2 = [WPosC, SPosC, NPosC]



    for j in range(nBuilds):
        _, FWy, poi_y = FrontZone(j, windzone, dd, lengthBi, widthBi, heights, buildCenter, z, y_wall_list, y_dir, Lf,
                                  theta, rad,
                                  y_pois, y_poisAlt, y_poisAlt2, nx, ny)

        FWx, _, poi_x = FrontZone(j, windzone, dd, lengthBi, widthBi, heights, buildCenter, z, x_wall_list, x_dir, Lf,
                                  theta, rad,
                                  x_pois, x_poisAlt, x_poisAlt2, nx, ny)
        for i in range(len(FWy)):
             selInd = FWy[i][(FWy[i][:, 1] > np.min(poi_y[:, 1])) & (FWy[i][:, 1] < np.max(poi_y[:, 1]))]
             sel = np.array([list(t) for t in set(tuple(element) for element in selInd)])
             if len(sel) >= 1:
                 vn[sel[:, 0], sel[:, 1], i] = 0.
        for i in range(len(FWx)):
            selInd = FWx[i][(FWx[i][:, 0] > np.min(poi_x[:, 0])) & (FWx[i][:, 0] < np.max(poi_x[:, 0]))]
            sel = np.array([list(t) for t in set(tuple(element) for element in selInd)])
            if len(sel) >= 1:
                un[sel[:, 0], sel[:, 1], i] = 0

    # ################################################
    # ### Calculate near back vorticity zones
    #
    print('Back')
    #### Arguments of dir walls for zones

    listSinner = []
    listWinner = []
    listEinner = []
    listNinner = []

    for i in np.arange(0, nBuilds):
        listSub = [x for x in listS[i]]
        listSinner.append(np.array(listSub))
        listSub = [x for x in listW[i]]
        listWinner.append(np.array(listSub))
        listSub = [x for x in listE[i]]
        listEinner.append(np.array(listSub))
        listSub = [x for x in listN[i]]
        listNinner.append(np.array(listSub))


    upB = dd + 90
    if dd < 90.:
        lowB = (360 + dd) - 90
    else:
        lowB = dd - 90

    if (dd >= 45.) and (dd <= 135.):
        Dir = 0
        Dir2 = 1
        if dd > 91:
            sub_zone = 1
        elif dd < 89:
            sub_zone = -1
        else:
            sub_zone = -2
    elif (dd > 135.) and (dd < 225.):
        Dir = 1
        Dir2 = 0
        if dd > 181:
            sub_zone = 1
        elif dd < 179:
            sub_zone = -1
        else:
            sub_zone = 2
    elif (dd >= 225.) and (dd <= 315.):
        Dir = 0
        Dir2 = 1
        if dd > 271:
            sub_zone = 1
        elif dd < 269:
            sub_zone = -1
        else:
            sub_zone = 2
    elif (dd > 315.) or (dd < 45.):
        Dir = 1
        Dir2 = 0
        if (dd > 1.) and (dd < 45.):
            sub_zone = 1
        elif (dd < 359.) and (dd > 315.):
            sub_zone = -1
        else:
            sub_zone = -2


    for j in np.arange(0, nBuilds):
        altPoints = np.array(list(SPosC[j]) + list(WPosC[j]) + list(EPosC[j]) + list(NPosC[j]))
        wallINDw = np.array(list(listSinner[j]) + list(listWinner[j]) + list(listEinner[j]) + list(listNinner[j]))
        NNpoints = NN_dirwalls(wallINDw, dirwalls, lowB, upB)

        if len(NNpoints) > 0:
            Poj = NNpoints[np.where(NNpoints[:, Dir] >= np.max(NNpoints[:, Dir]) - 5)]
            Poi = NNpoints[np.where(NNpoints[:, Dir] <= np.min(NNpoints[:, Dir]) + 5)]

            if sub_zone == 1:
                Poj = Poj[np.where(Poj[:, Dir2] == np.max(Poj[:, Dir2]))]
                Poi = Poi[np.where(Poi[:, Dir2] == np.min(Poi[:, Dir2]))]
            elif sub_zone == -1:
                Poj = Poj[np.where(Poj[:, Dir2] == np.min(Poj[:, Dir2]))]
                Poi = Poi[np.where(Poi[:, Dir2] == np.max(Poi[:, Dir2]))]
            elif sub_zone == -2:
                Poj = Poj[np.where(Poj[:, Dir2] == np.min(Poj[:, Dir2]))]
                Poi = Poi[np.where(Poi[:, Dir2] == np.min(Poi[:, Dir2]))]
            else: #sub_zone == 2
                Poj = Poj[np.where(Poj[:, Dir2] == np.max(Poj[:, Dir2]))]
                Poi = Poi[np.where(Poi[:, Dir2] == np.max(Poi[:, Dir2]))]

            if len(Poj) > 1:
                Poj = Poj[np.where(Poj[:, Dir] == np.max(Poj[:, Dir]))]
                Poj = np.array([list(t) for t in set(tuple(element) for element in Poj)])

            if len(Poi) > 1:
                Poi = Poi[np.where(Poi[:, Dir] == np.min(Poi[:, Dir]))]
                Poi = np.array([list(t) for t in set(tuple(element) for element in Poi)])

            Pois = np.array(list(Poi) + list(Poj))
        else:
            Pois = []

        NNindex, NNx, POi = BackZone(j, NN, dd, buildCenter, Dir, Dir2, 1, effWi, thetaN, rad, Pois, altPoints,
                                     wallINDw, nx, ny)

        #wallINDw = wallINDw[::2]
        #Buildregion_indexes = {tuple(d): 1 for sublist in Buildregion for d in sublist}.keys()
        Buildregion_indexes = {tuple(d): 1 for d in Buildregion[j]}.keys()

        for i in np.arange(0, len(NNindex)):


            #NNind = np.array(NNindex[i])

            NNind = NNindex[i]
            nni_points = {tuple(d): 1 for d in NNind}.keys()
            NNind = np.array(list(nni_points - Buildregion_indexes))  # remove points in Buildregion from NNind
            if len(NNind) >0 :
                dlN = np.min(distance.cdist(NNind, wallINDw), axis=1)
                ### Calculate near away zone

                un[NNind[:, 0], NNind[:, 1], i] = -u[NNind[:, 0], NNind[:, 1], i] * (1 - dlN/NNx[i]) ** 2
                vn[NNind[:, 0], NNind[:, 1], i] = -v[NNind[:, 0], NNind[:, 1], i] * (1 - dlN / NNx[i]) ** 2

    ####### Flow regime #######
    #

    iSH = np.zeros(np.size(buildGrid3D, 0))
    jSH = np.zeros(np.size(buildGrid3D, 1))
    FlowRegimeM = np.zeros_like(buildGrid3D)

    runi = [0]
    print('FlowRegime')
    ratioSW = 2.

    for i in np.arange(0, len(iSH)):
        runi.append(0)
        # this is the most common value of rooflevel so we save it
        rooflevel_0 = np.int32(np.argwhere(buildGrid3D[i, (runi[-1] - 1), :] == 1.) * dy)
        runi_latest = runi[-1]
        for j in np.arange(0, len(jSH)):
            #calculation rooflevels is slow so we only recalculate it when it changes
            if runi[-1] == 0:
                rooflevel = rooflevel_0
            elif runi[-1] != runi_latest:
                runi_latest = runi[-1]
                rooflevel = np.int32(np.argwhere(buildGrid3D[i, (runi[-1] - 1), :] == 1.) * dy)

            if (buildGrid3D[i, j, 0] == 0.) and (buildGrid3D[i, j - 1, 0] == 1.) and (buildGrid3D[i, j + 1, 0] == 0.):
                jSHn = np.count_nonzero(buildGrid3D[i, runi[-1]:j, 0] == 1.) * dy
                jSHn = jSHn

                if runi[-1] == 0:
                    continue
                else:
                    si = rooflevel[0] * (1. + 1.4 * (jSHn / rooflevel[0]) ** (1. / 4.))

                    if (jSHn / rooflevel[0] > ratioSW):
                        ss = rooflevel[0] * 1.55
                    else:
                        ss = rooflevel[0] * (1.25 + 0.15 * (jSHn / rooflevel[0]))

                    if jSHn > si:
                        FlowRegimeM[i, runi[-1]:j, 0:rooflevel[0][0]] = 1
                    elif (jSHn > ss):
                        FlowRegimeM[i, runi[-1]:j, 0:rooflevel[0][0]] = 2
                        if ((j-runi[-1]) < 45) and (any(x == 3 for x in FlowRegimeM[i-1, runi[-1]:j, 0])):
                            FlowRegimeM[i, runi[-1]:j, 0:rooflevel[0][0]] = 3
                            for k in np.arange(runi[-1], (j)):
                                for l in np.arange(0, rooflevel[0][0]):
                                    dlS = (j - k) * (1.)

                                    vn[i, k, l] = v[i, k, l]

                                    un[i, k, l] = u[i, j, rooflevel[0][0]] * np.cos(
                                        (dirwalls[i, runi[-1]]-90) * rad) * (-1.) * (dlS / (jSHn / 2.)) * (
                                                (jSHn - dlS) / (jSHn / 2.))
                                    wn[i, k, l] = np.abs(u[i, j, rooflevel[0][0]] / 2. * (1 - dlS / (jSHn / 2.))) * (
                                            1 - (jSHn - dlS) / (jSHn / 2.))
                        else:
                            continue
                    else:
                        FlowRegimeM[i, runi[-1]:j, 0:rooflevel[0][0]] = 3
                        for k in np.arange(runi[-1], (j)):
                            for l in np.arange(0, rooflevel[0][0]):
                                dlS = (j - k) * (1.)

                                vn[i, k, l] = v[i, k, l]

                                un[i, k, l] = u[i, j, rooflevel[0][0]] * np.cos((dirwalls[i, runi[-1]] - 90) * rad) * (
                                    -1.) * (dlS / (jSHn / 2.)) * (
                                                          (jSHn - dlS) / (jSHn / 2.))
                                wn[i, k, l] = np.abs(u[i, k, rooflevel[0][0]] / 2. * (1 - dlS / (jSHn / 2.))) * (
                                                1 - (jSHn - dlS) / (jSHn / 2.))

            elif (buildGrid3D[i, j, 0] == 0.) and (buildGrid3D[i, j - 1, 0] == 0.) and (buildGrid3D[i, j + 1, 0] == 1.):
                runi.append(j + 1)
            else:
                continue


    for j in np.arange(0, len(jSH)):
        runi.append(0)
        rooflevel_0 = np.int32(np.argwhere(buildGrid3D[(runi[-1] - 1), j, :] == 1.) * dx)
        runi_latest = runi[-1]
        for i in np.arange(0, len(iSH)):
            if runi[-1] == 0:
                rooflevel = rooflevel_0
            elif runi[-1] != runi_latest:
                rooflevel = np.int32(np.argwhere(buildGrid3D[(runi[-1] - 1), j, :] == 1.) * dx)

            if (buildGrid3D[i, j, 0] == 0.) and (buildGrid3D[i - 1, j, 0] == 1.) and (buildGrid3D[i + 1, j, 0] == 0.):
                iSHn = np.count_nonzero(buildGrid3D[runi[-1]:i, j, 0] == 1.) * dx
                iSHn = iSHn * 1.

                if runi[-1] == 0:
                    continue
                else:
                    si = rooflevel[0] * (1. + 1.4 * (iSHn / rooflevel[0]) ** (1. / 4.))

                    if (iSHn / rooflevel[0] > ratioSW):
                        ss = rooflevel[0] * 1.55
                    else:
                        ss = rooflevel[0] * (1.25 + 0.15 * (iSHn / rooflevel[0]))

                    if iSHn > si:
                        FlowRegimeM[runi[-1]:i, j, 0:rooflevel[0][0]] = 1
                    elif (iSHn > ss):
                        FlowRegimeM[runi[-1]:i, j, 0:rooflevel[0][0]] = 2
                        if ((i-runi[-1]) < 45) and (any(x == 3 for x in FlowRegimeM[runi[-1]:i, j-1, 0])):

                            FlowRegimeM[runi[-1]:i, j, 0:rooflevel[0][0]] = 3
                            for k in np.arange(runi[-1], (i)):
                                for l in np.arange(0, rooflevel[0][0]):
                                    dlS = (i - k) * (1.)

                                    if (un[k, j, l] >= 0.) and (u[k, j, l] >= 0.):
                                        un[k, j, l] = u[k, j, l]

                                    if dirwalls[runi[-1] - 1, j] >= 180.:
                                        vn[k, j, l] = (-1.) * v[i, j, rooflevel[0][0]] * np.cos(
                                                (360 - dirwalls[runi[-1], j]) * rad) * (dlS / (iSHn / 2.)) * (
                                                    (iSHn - dlS) / (iSHn / 2.))
                                    else:
                                        vn[k, j, l] = (-1.) * v[i, j, rooflevel[0][0]] * np.cos(
                                            (dirwalls[runi[-1], j]) * rad) * (dlS / (iSHn / 2.)) * (
                                                              (iSHn - dlS) / (iSHn / 2.))
                                    wn[k, j, l] = np.abs(u[k, j, rooflevel[0][0]] / 2. * (1 - dlS / (iSHn / 2.))) * (
                                            1 - (iSHn - dlS) / (iSHn / 2.))
                        else:
                            continue
                    else:
                        FlowRegimeM[runi[-1]:i, j, 0:rooflevel[0][0]] = 3
                        for k in np.arange(runi[-1], (i)):
                            for l in np.arange(0, rooflevel[0][0]):
                                dlS = (i - k) * (1.)

                                if (un[k, j, l] >= 0.) and (u[k, j, l] >= 0.):
                                    un[k, j, l] = u[k, j, l]

                                if dirwalls[runi[-1] - 1, j] >= 180.:
                                    vn[k, j, l] = (-1.) * v[i, j, rooflevel[0][0]] * np.cos(
                                        (360 - dirwalls[runi[-1], j]) * rad) * (dlS / (iSHn / 2.)) * (
                                                          (iSHn - dlS) / (iSHn / 2.))
                                else:
                                    vn[k, j, l] = (-1.) * v[i, j, rooflevel[0][0]] * np.cos(
                                        (dirwalls[runi[-1], j]) * rad) * (dlS / (iSHn / 2.)) * (
                                                          (iSHn - dlS) / (iSHn / 2.))
                                wn[k, j, l] = np.abs(u[i, j, rooflevel[0][0]] / 2. * (1 - dlS / (iSHn / 2.))) * (
                                    1 - (iSHn - dlS) / (iSHn / 2.))

            elif (buildGrid3D[i, j, 0] == 0.) and (buildGrid3D[i - 1, j, 0] == 0.) and (buildGrid3D[i + 1, j, 0] == 1.):
                runi.append(i + 1)
            else:
                continue

    ##############################
    ### Vegetation calculation
    sigmaF = densityplants

    ###################
    ### Clarke table

    fu = np.ones([8, 18])
    fu[0, :] = fu[0, :] * np.zeros([1, 18])
    fu[1, :] = fu[1, :] * np.array([2.5745, 3.0191, 3.5983, 4.3057, 5.1257, 5.5060, 6.0998, 6.5872, 7.5000, 9.8253,
                                    14.1527, 27.3780, 32.4586, 36.9598, 37.0471, 35.3041, 33.5989, 31.0125])
    fu[2, :] = fu[2, :] * np.array([3.7309, 4.3519, 5.1381, 6.0595, 7.0802,
                                    7.5410, 8.2531, 8.8398, 10.0000, 13.2325,
                                    19.3497, 32.5374, 37.8812, 43.8711, 45.7201,
                                    45.6004, 44.6743, 42.2457])
    fu[3, :] = fu[3, :] * np.array([5.1568, 5.9570, 6.9294, 8.0176, 9.1767,
                                    9.6906, 10.4816, 11.1374, 12.5000, 16.6983,
                                    24.3212, 36.6732, 41.9995, 48.5354, 51.2707,
                                    52.1928, 52.1922, 51.2884])
    fu[4, :] = fu[4, :] * np.array([6.8637, 7.8153, 8.9264, 10.1216,
                                    11.3672, 11.9153, 12.7596, 13.4658, 15.0000,
                                    19.9384, 28.8918, 40.3255, 45.5198, 52.1303,
                                    55.1752, 56.5433, 56.9932, 56.9487])
    fu[5, :] = fu[5, :] * np.array([8.8081, 9.8621, 11.0550, 12.3164,
                                    13.6160, 14.1873, 15.0701, 15.8151,
                                    17.5000, 23.2638, 32.9446, 43.7321,
                                    48.7646, 55.2641, 58.3570, 59.8631, 60.4794, 60.7915])
    fu[6, :] = fu[6, :] * np.array([13.1106, 14.2549, 15.5226, 16.8484,
                                    18.2123, 18.8143, 19.7520, 20.5557,
                                    22.5000, 29.8902, 40.2728, 50.1720, 54.8977,
                                    61.0420, 63.9933, 65.4705, 66.1191, 66.5662])
    fu[7, :] = fu[7, :] * np.array([31.6297, 32.8561, 34.2161, 35.6470,
                                    37.1378, 37.8068, 38.8690, 39.8100, 42.5000,
                                    56.0756, 65.7429, 73.9294, 77.8518, 82.8322,
                                    85.1030, 86.1589, 86.5728, 86.7745])

    #bilinear interpolation for the Clarkefunction
    U = np.zeros_like(fu[0, :])
    for i in np.arange(1, np.shape(fu)[0]):
        for j in np.arange(1, np.shape(fu)[1]):
            U[i] = fu[i - 1, j - 1] + (fu[i, j - 1] - fu[i - 1, j - 1]) * dx + \
                   (fu[i - 1, j] - fu[i - 1, j - 1]) * dy + \
                   (fu[i, j] - fu[i - 1, j] + fu[i - 1, j - 1] - fu[i, j - 1]) * dx * dy

    ###### Wind speed inside vegetation
    indices = np.transpose(canopyIndex)
    indices = indices.astype(np.int32)
    zrefV = np.int32(zref)
    for i, j, k in indices:
        un[i, j, k] = sigmaF * np.max(np.divide(u[i, j, zrefV], U[U >= 0.05])) * 0.83 + (1 - sigmaF) * un[i, j, k]

        vn[i, j, k] = sigmaF * np.max(np.divide(v[i, j, zrefV], U[U >= 0.05])) * 0.83 + (1 - sigmaF) * vn[i, j, k]

        wn[i, j, k] = sigmaF * np.max(np.divide(w[i, j, zrefV], U[U >= 0.05])) * 0.83 + (1 - sigmaF) * wn[i, j, k]


    # un[canopyIndex] = sigmaF * un[canopyIndex] * 0.1 + (1 - sigmaF) * un[canopyIndex]
    # vn[canopyIndex] = sigmaF * vn[canopyIndex] * 0.1 + (1 - sigmaF) * vn[canopyIndex]
    # wn[canopyIndex] = sigmaF * wn[canopyIndex] * 0.1 + (1 - sigmaF) * wn[canopyIndex]

    un[buildIndex] = 0.
    vn[buildIndex] = 0.
    wn[buildIndex] = 0.

    return buildIndex, buildGrid3D, canopyIndex, un, vn, wn, u, v, w, nz