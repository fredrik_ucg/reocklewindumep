#import numpy as np
#import rasterio

#from WindModel_Rockle import build_buildingmask


def perpare_rasters(building_mask,wall_height,wall_dir,vegetation_mask):
    building_mask = (building_mask * -1) + 1

    wall_height[wall_height < 0] = 0
    wall_height = wall_height[::-1]

    wall_dir[wall_dir < 0] = 0
    wall_dir = wall_dir[::-1]

    building_mask = building_mask[::-1]

    building_mask[0, :] = 1.
    building_mask[:, 0] = 1.
    building_mask[-1, :] = 1.
    building_mask[:, -1] = 1.

    vegetation_mask = vegetation_mask[::-1]

    vegetation_mask[0, :] = 1.
    vegetation_mask[:, 0] = 1.
    vegetation_mask[-1, :] = 1.
    vegetation_mask[:, -1] = 1.

    rows, cols = building_mask.shape
    if rows < cols:
        nx = cols  # Number of steps in space(x)
        ny = cols  # Number of steps in space(y)
    elif rows == cols:
        nx = rows  # Number of steps in space(x)
        ny = cols  # Number of steps in space(y)
    else:
        nx = rows  # Number of steps in space(x)
        ny = rows  # Number of steps in space(y)

    return building_mask,vegetation_mask,wall_height,wall_dir,nx,ny

