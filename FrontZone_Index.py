import numpy as np
from math import atan2#, hypot
from InnerRegions import InnerRegions

#@profile
def FrontZone(j, ddM, dd, lengthBi, widthBi, heights, buildCenter, z, listDir, Dirs, Lf, theta, rad, Pois,
              PoisAlt, PoisAlt2, nx, ny):

    Dir, Dir2, Dir3 = Dirs
    FWx = np.zeros([len(z)])
    FWy = np.zeros([len(z)])

    if len(Pois[j]) >= 1:
        PO1 = np.array(Pois[j])
        POsi = PO1[np.where(PO1[:, Dir2] == np.min(PO1[:, Dir2]))]
        POsj = PO1[np.where(PO1[:, Dir2] == np.max(PO1[:, Dir2]))]
    else:
        POsi = np.empty((1,2,))
        POsj = np.empty((1,2,))
        POsi[:] = np.nan
        POsj[:] = np.nan

    if len(PoisAlt[j]) >= 1:
        POalt1 = np.array(PoisAlt[j])
    else:
        POalt1 = np.empty((1, 2,))
        POalt1[:] = np.nan

    if len(PoisAlt2[j]) >= 1:
        POalt2 = np.array(PoisAlt2[j])
    else:
        POalt2 = np.empty((1, 2,))
        POalt2[:] = np.nan


    if Dir3 == 1:
        POalti = POalt1[np.where(POalt1[:, Dir2] == np.min(POalt1[:, Dir2]))]
        POaltj = POalt2[np.where(POalt2[:, Dir2] == np.max(POalt2[:, Dir2]))]

    elif Dir3 == 2:
        POalti = POalt1[np.where(POalt1[:, Dir] == np.min(POalt1[:, Dir]))]
        POaltj = POalt2[np.where(POalt2[:, Dir] == np.min(POalt2[:, Dir]))]

    else:
        POalti = POalt1[np.where(POalt1[:, Dir] == np.max(POalt1[:, Dir]))]
        POaltj = POalt2[np.where(POalt2[:, Dir] == np.max(POalt2[:, Dir]))]


    POalti = np.array([list(t) for t in set(tuple(element) for element in POalti)])
    POsi = np.array([list(t) for t in set(tuple(element) for element in POsi)])

    POaltj = np.array([list(t) for t in set(tuple(element) for element in POaltj)])
    POsj = np.array([list(t) for t in set(tuple(element) for element in POsj)])

    POsiA = np.array(list(POsi) + list(POalti) + list(POsj) + list(POaltj))
    POsiA = POsiA[~np.any(np.isnan(POsiA), axis=1)]
    POsiA = np.int_(POsiA)


    if Dir == 0:
        POsiA = sorted(POsiA, key=lambda x: x[1])
        POsiA = np.array(POsiA)
    else:
        POsiA = sorted(POsiA, key=lambda x: x[0])
        POsiA = np.array(POsiA)


    for k in np.arange(0, len(z)):
        if z[k] > 0.6 * heights[j]:
            break
        else:
            if Dir == 1:
                FWx[k] = Lf[j] * np.cos((listDir[j] - dd) * rad) ** 2 * np.sqrt(1 - (z[k] / (0.6 * heights[j])) ** 2)
            else:
                FWy[k] = Lf[j] * np.cos((listDir[j] - dd) * rad) ** 2 * np.sqrt(1 - (z[k] / (0.6 * heights[j])) ** 2)

    FWxindex = []
    FWyindex = []
    FWxindexA = []
    FWyindexA = []

    if Dir == 1:
        FWxx = np.trim_zeros(FWx)
        for i in np.arange(0, len(FWxx)):
            if i >= len(FWxx):
                continue
            elif len(POsiA) <= 1:
                continue
            else:
                for l in np.arange(0, len(POsiA) - 1):
                    FWxindexA.append(InnerRegions(i, j, nx, ny, np.array(POsiA[l]), np.array(POsiA[l+1]), FWxx[i], theta,
                                                  (270 - ((atan2((np.array(POsiA[l + 1][1]) - np.array(POsiA[l][1])),
                                                                 (np.array(POsiA[l + 1][0]) - np.array(
                                                                     POsiA[l][0]))) / rad) + 90)) * rad,
                                                  None, None, None, dd))
                FWxindex.append(np.vstack((FWxindexA)))

    else:
        FWyx = np.trim_zeros(FWy)
        for i in np.arange(0, len(FWyx)):
            if i >= len(FWyx):
                continue
            elif len(POsiA) <= 1:
                continue
            else:
                for l in np.arange(0, len(POsiA) - 1):
                    FWyindexA.append(InnerRegions(i, j, nx, ny, np.array(POsiA[l]), np.array(POsiA[l+1]), FWyx[i], theta,
                                                  (270 - ((atan2((np.array(POsiA[l + 1][1]) - np.array(POsiA[l][1])),
                                                                 (np.array(POsiA[l + 1][0]) - np.array(
                                                                     POsiA[l][0]))) / rad) + 90)) * rad,
                                                  None, None, None, dd))
                FWyindex.append(np.vstack((FWyindexA)))


    return FWxindex, FWyindex, POsiA