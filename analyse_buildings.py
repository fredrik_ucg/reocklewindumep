import numpy
import numpy as np

def NN_dirwalls(lines, dirwalls, lowerB, upperB):
    listDirInd = []
    wallS = np.where(np.logical_and(np.logical_or(np.greater_equal(dirwalls, lowerB), np.less_equal(dirwalls, upperB)),
                                    np.not_equal(dirwalls, 0.)))
    wallIndex = np.column_stack((np.stack(wallS)[0], np.stack(wallS)[1]))
    for i in range(len(lines)):
        equal_args = np.argwhere(np.bitwise_and.reduce(wallIndex == lines[i], axis=1)).flatten()
        wi = wallIndex[equal_args].flatten()
        if len(wi) > 0:
            listDirInd.append(wi)
    if len(listDirInd) > 0:
        listDirInd= np.vstack(listDirInd)

    return listDirInd

def _list_of_dirwalls(lines, wallDir, listDirInd, listDir, dirwalls):
    for i in range(len(lines)):
        sublist = []
        sublistW = []
        for k in range(len(lines[i])):
            if k == 216:
                pass
            for j in range(len(wallDir)):
                if j == 670:
                    pass
                if (wallDir[j, 0] == lines[i][k, 0]) and (wallDir[j, 1] == lines[i][k, 1]):
                    sublist.append(wallDir[j])#lines[i][k])
                    sublistW.append(dirwalls[wallDir[j][0], wallDir[j][1]])
                else:
                    continue
        listDirInd.append(sublist)
        listDir.append(sublistW)
    return listDirInd, listDir

def list_of_dirwalls(lines, wallDir, listDirInd, listDir, dirwalls):
    for i in range(len(lines)):
        sublist = []
        sublistW = []
        for k in range(len(lines[i])):
            equal_args = np.argwhere(np.bitwise_and.reduce(wallDir == lines[i][k], axis=1)).flatten()
            if len(equal_args)>0:
                sublist.append(wallDir[equal_args].flatten())
                sublistW.append(dirwalls[wallDir[equal_args][0][0],wallDir[equal_args][0][1]])
            #for j in range(len(wallDir)):
            #    if (wallDir[j, 0] == lines[i][k, 0]) and (wallDir[j, 1] == lines[i][k, 1]):
            #        sublist.append(wallDir[j])#lines[i][k])
            #        sublistW.append(dirwalls[wallDir[j][0], wallDir[j][1]])
            #    else:
            #        continue
        listDirInd.append(sublist)
        listDir.append(sublistW)
    return listDirInd, listDir


def cornerPoints(operator_dict, n, listDir, DirPoi, Centre, listDirW, Dir, Dir2, Dir3, oper, m):
    for i in np.arange(0, n):
        wallIndexN = np.array(listDir[i])
        if len(wallIndexN) == 0:
            DirPoi.append([])
        else:
            if Dir3 == 0:
                idxy = np.where(wallIndexN[:, Dir] >= max(wallIndexN[:, Dir]) - 1)
                subPoi = wallIndexN[idxy]
            else:
                idxx = np.where(wallIndexN[:, Dir] <= min(wallIndexN[:, Dir]) + 1)
                subPoi = wallIndexN[idxx]

            if operator_dict[oper](listDirW[i], m):
                idxy2 = np.where(subPoi[:, Dir2] == max(subPoi[:, Dir2]))
            else:
                idxy2 = np.where(subPoi[:, Dir2] == min(subPoi[:, Dir2]))

            DirPoi.append(subPoi[idxy2])
            DirPoi[i] = [list(t) for t in set(tuple(element) for element in DirPoi[i])]
    return DirPoi


def cornerPoints2(operator_dict, n, listDir, DirPoi, Centre, listDirW, Dir, Dir2, Dir3, oper, m):
    for i in np.arange(0, n):
        wallIndexN = np.array(listDir[i])
        if len(wallIndexN) == 0:
            DirPoi.append([])
        else:
            if operator_dict[oper](listDirW[i], m):
                idxy = np.where(wallIndexN[:, Dir] == max(wallIndexN[:, Dir]))
                subPoi = wallIndexN[idxy]
            else:
                idxx = np.where(wallIndexN[:, Dir] == min(wallIndexN[:, Dir]))
                subPoi = wallIndexN[idxx]

            if Dir3 == 0:
                idxy2 = np.where(subPoi[:, Dir2] == max(subPoi[:, Dir2]))
            else:
                idxy2 = np.where(subPoi[:, Dir2] == min(subPoi[:, Dir2]))

            DirPoi.append(subPoi[idxy2])
            DirPoi[i] = [list(t) for t in set(tuple(element) for element in DirPoi[i])]
    return DirPoi

