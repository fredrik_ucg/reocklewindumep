import numpy as np
### inner regions ###
from skimage.draw import polygon, polygon_perimeter
from skimage.measure import label, regionprops

def Canopy3D(i, nx, ny, vegIndex, heights):

    # This is the function calculating the coordinates of vegetation type, respectively
    # 2018-Oct-10
    # Sandro Oswald, sandro.oswald@boku.ac.at
    # Vienna Urban Climate Group
    # University of natural sciences (BOKU)

    Y, X = vegIndex[i].vertices.T

    NNstack = np.column_stack((X.astype(np.int32), Y.astype(np.int32)))

    border_points_x = np.array([p[0] for p in NNstack] + [NNstack[0][0]])
    border_points_y = np.array([p[1] for p in NNstack] + [NNstack[0][1]])

    image = np.zeros((nx + 1, ny + 1))

    # polygon() calculates the indices of a filled polygon
    # one would expect this to be inner+border but apparently it is (inner+border)/2
    filled_rr, filled_cc = polygon(border_points_x, border_points_y)

    # set the image to white at these points
    image[filled_rr, filled_cc] = 1

    # polygon_perimeter() calculates the indices of a polygon perimiter (i.e. border)
    #border_rr, border_cc = polygon_perimeter(border_points_x, border_points_y)

    # exclude border, by setting it to black
    #image[border_rr, border_cc] = 0

    # label() detects connected patches of the same color and enumerates them
    label_img, num_regions = label(image, background=0, return_num=True)

    # regionprops() takes a labeled image and computes some measures for each region
    regions = regionprops(label_img, coordinates='xy')
    inner_region = regions[0]

    #zi = np.round(heights[i]).astype(int)

    return inner_region#, zi