from __future__ import print_function

import tempfile
import time

import numpy as np
import rasterio
import matplotlib.pyplot as plt
#from osgeo import gdal
# from mpl_toolkits.mplot3d import axes3d
# import Solweig_v2015_metdata_noload as metload


import FlowZones_Rockle as FlowZones
import windmodel_solver as windmodel_solver
from load_data import perpare_rasters

########################
def findwalls(a, walllimit):
    # This function identifies walls based on a DSM and a wall-height limit
    # Walls are represented by outer pixels within building footprints
    #
    # Fredrik Lindberg, Goteborg Urban Climate Group
    # fredrikl@gvc.gu.se
    # 20150625

    col = a.shape[0]
    row = a.shape[1]
    walls = np.zeros((col, row))
    domain = np.array([[0, 1, 0], [1, 0, 1], [0, 1, 0]])
    for i in np.arange(1, row-1):
        for j in np.arange(1, col-1):
            dom = a[j-1:j+2, i-1:i+2]
            #walls[j, i] = np.min(dom[np.where(domain == 1)])  # new 20171006
            walls[j, i] = np.max(dom[np.where(domain == 1)])  # new 20171006

    #walls = np.copy(a - walls)  # new 20171006
    walls = np.copy(walls - a)  # new 20171006
    walls[(walls < walllimit)] = 0

    walls[0:walls .shape[0], 0] = 0
    walls[0:walls .shape[0], walls .shape[1] - 1] = 0
    walls[0, 0:walls .shape[0]] = 0
    walls[walls .shape[0] - 1, 0:walls .shape[1]] = 0

    return walls

##################
def build_buildingmask(dsm_raster,dem_raster):
    diff_raster = dsm_raster - dem_raster
    diff_raster[diff_raster < 2] = 0
    diff_raster[diff_raster >= 2] = 1
    return diff_raster


##################
def load_data(buildingDSM, terrainDEM, wallHeight, wallDir, canpyDSM, buildingMask):
    with rasterio.open(buildingDSM) as src:
        buildingsDSM = src.read(1).astype(rasterio.float32)
        raster_affine = src.transform
        raster_profile = src.profile
        scale = raster_affine.to_gdal()[1]

    if terrainDEM:
        with rasterio.open(terrainDEM) as src:
            terrainDEM = src.read(1).astype(rasterio.float32)
    else:
        terrainDEM = np.zeros_like(buildingsDSM)

    #with rasterio.open(wallHeight) as src:
    wall_height = wallHeight #src.read(1).astype(rasterio.float32)

    with rasterio.open(wallDir) as src:
        wall_dir = src.read(1).astype(rasterio.float32)

    with rasterio.open(canpyDSM) as src:
        vegetation_mask = src.read(1).astype(rasterio.float32)
        vegetation_mask[vegetation_mask <= 1] = 1
        vegetation_mask[vegetation_mask > 1] = 2

    if buildingMask is None:
        building_mask = build_buildingmask(buildingsDSM,terrainDEM)
    else:
        with rasterio.open(buildingMask) as src:
            building_mask = src.read(1).astype(rasterio.float32)
            building_mask[building_mask < 2] = 0
            building_mask[building_mask >= 2] = 1

    building_mask, vegetation_mask, wall_height, wall_dir, nx, ny = perpare_rasters(building_mask, wall_height, wall_dir, vegetation_mask)


    return building_mask, vegetation_mask, wall_height, wall_dir, nx, ny, scale, raster_profile


#####################
def WindModel_Rockle(nx, ny, Ws, Wd, zref, builds, walls, dirwalls, vegheight, vegetation, densityplants, voxelheight,iterations = 15,
                     write_results_to_disk=False):
    start_windmodel = time.time()

    # This is the main function calculating the wind scheme model
    # 2017-Sept-05
    # Sandro Oswald, sandro.oswald@boku.ac.at
    # Vienna Urban Climate Group
    # University of natural sciences (BOKU)




    print(Ws,Wd,'Wind')
    Wd = Wd % 360 #Winddirection has to be between 0-360


    #rad = np.pi/180.



    dx = voxelheight  # Width of space step(x)
    dy = voxelheight  # Width of space step(y)

    buildIndex, buildGrid3D, canopyIndex, un, vn, wn, u, v, w, nz = FlowZones.FlowZonesR(builds, dirwalls, walls, Ws, Wd, vegheight,
                                                                            vegetation, densityplants, zref, nx, ny, dx, dy)

    buildIndexB = np.stack(buildIndex).astype(np.int32)
    vegIndexC = np.stack(canopyIndex).astype(np.int32)
    indices = np.transpose(np.where(buildGrid3D == 1.))
    indices = indices[indices[:, 0] > 0]
    indices = indices[indices[:, 1] > 0]
    indices = indices[indices[:, 2] > 0]
    indices = indices[indices[:, 0] < nx - 1]
    indices = indices[indices[:, 1] < ny - 1]
    indices = indices[indices[:, 2] < nz - 1]
    indices = indices.astype(np.int32)

    start_sor = time.time()
    u, v, w, x, y, z, e, lambdaM1 = windmodel_solver.solver(dx, dy, nx, ny, nz, un, vn, wn, u, v, w, buildIndexB, indices, iterations)


    print(f"total_windmodel time {time.time()-start_windmodel}")
    print(f"time spent iterating {time.time()-start_sor}")
    if write_results_to_disk:
        outfile = tempfile.NamedTemporaryFile(suffix='.npz',delete=False)
        outfile.close()
        np.savez(outfile.name,u=u, v=v, w=w, lambdaM1=lambdaM1, x=x, y=y, z=z, buildIndex=buildIndex,
                 buildIndexB=buildIndexB, un=un, vn=vn, wn=wn, nz=nz)
        return outfile.name

    else:
        return u, v, w, lambdaM1, x, y, z, e, buildIndex, buildIndexB, vegIndexC, un, vn, wn, nz


dsm = 'F:/Users/soswald/PycharmProjects/RoeckleWind/Data/KRbig_DSM_11.tif' ##KRbig_DSM_11  DSM_KRbig
dsmr = rasterio.open(dsm)
buildingsDSM = dsmr.read(1).astype(rasterio.float32) #np.array(dsm.GetRasterBand(1).ReadAsArray()) #
wallH = findwalls(buildingsDSM, 3.)  #'F:/Users/soswald/PycharmProjects/RoeckleWind/Data/WallHeight_Goe.tif'

dem = 'F:/Users/soswald/PycharmProjects/RoeckleWind/Data/DEM_KRbig.tif'
cdsm = 'F:/Users/soswald/PycharmProjects/RoeckleWind/Data/Goe_CDSM.tif'
dsmrc = rasterio.open(cdsm)
vegheight = dsmrc.read(1).astype(rasterio.float32)
vegheight = vegheight[::-1]

wallD = 'F:/Users/soswald/PycharmProjects/RoeckleWind/Data/KRbig_DSM_wallaspect.tif'  ##KRbig_DSM_wallaspect  WallAspect_Goe

builds, vegetation, walls, dirwalls, nx, ny, scale, raster_profile = load_data(dsm, dem, wallH, wallD, cdsm, None)

Ws = 1.8
Wd = 330.
zref = 25.

voxelheight = scale
densityplants = 0.9


u, v, w, lambdaM1, x, y, z, e, buildIndex, buildIndexB, vegIndexC, un, vn, wn, nz = WindModel_Rockle(nx, ny, Ws, Wd, zref, builds,
                                                                                       walls, dirwalls, vegheight, vegetation, densityplants,
                                                                                       voxelheight, 5, False)


u[buildIndex] = 0.
v[buildIndex] = 0.
w[buildIndex] = 0.
uplot = np.zeros([nx, ny, nz])
vplot = np.zeros([nx, ny, nz])
wplot = np.zeros([nx, ny, nz])

vplot[0:nx, :, :] = 0.5 * (v[0:nx, :, :] + v[1:nx + 1, :, :])
uplot[:, 0:ny, :] = 0.5 * (u[:, 0:ny, :] + u[:, 1:ny + 1, :])
wplot[:, :, 0:nz] = 0.5 * (w[:, :, 0:nz] + w[:, :, 1:nz + 1])
Len = np.sqrt(uplot ** 2 + vplot ** 2 + wplot ** 2 + 2.2e-16)

uplot[buildIndex] = 0.
vplot[buildIndex] = 0.
wplot[buildIndex] = 0.
Len[buildIndex] = 0.

u2D = uplot[:, :, 3]
v2D = vplot[:, :, 3]
w2D = vplot[50, :, :]
Len2D = Len[:, :, 3]

uplot = np.divide(uplot, Len)
vplot = np.divide(vplot, Len)
wplot = np.divide(wplot, Len)


X3, Y3, Z3 = np.meshgrid(x, y, z)
X2, Y2 = np.meshgrid(x, y)

##############################
## Plots
# Create a mask
mask = np.zeros(Len2D.shape, dtype=bool)
mask[buildIndexB[0], buildIndexB[1]] = True
mask[vegIndexC[0], vegIndexC[1]] = True

#u2D = np.ma.array(u2D, mask=mask)
#v2D = np.ma.array(v2D, mask=mask)
clevs = np.arange(0.1, 1.6, 0.02)
m = plt.subplot()
cs = m.contourf(Len2D, clevs, cmap=plt.cm.get_cmap('gist_rainbow'))
#m.imshow(e[:,:,3], alpha=0.5)
cs.cmap.set_under('k')
cs.cmap.set_over('white', alpha=0.35)
plt.colorbar(cs)
plt.show()

fig, ax2 = plt.subplots()
lw = 3. * Len2D / Len2D.max()
strm = ax2.streamplot(X2, Y2, uplot[:, :, 17], vplot[:, :, 17], color=Len[:, :, 17], density=11.0, linewidth=0.8, cmap=plt.cm.get_cmap('gist_rainbow')) #density=5.6, color='r', linewidth=lw)
#ax2.set_title('Varying Line Width')
cbar = fig.colorbar(strm.lines)
cbar.set_clim(0.02,2.5)
ax2.imshow(~mask, alpha=0.6, origin="lower",
          interpolation='spline16',
           cmap='gray', aspect='auto')
#ax2.set_aspect('equal')
#plt.gca().invert_yaxis()
plt.show()

fig, ax2 = plt.subplots()
lw = 3. * Len2D / Len2D.max()
strm = ax2.streamplot(X2, Y2, u2D, v2D, color=Len2D, density=11.0, linewidth=0.8, cmap=plt.cm.get_cmap('gist_rainbow')) #density=5.6, color='r', linewidth=lw)
#ax2.set_title('Varying Line Width')
cbar = fig.colorbar(strm.lines)
cbar.set_clim(0.02,2.5)
ax2.imshow(~mask, alpha=0.6, origin="lower",
          interpolation='spline16',
           cmap='gray', aspect='auto')
#ax2.set_aspect('equal')
#plt.gca().invert_yaxis()
plt.show()

fig1, ax1 = plt.subplots()
ax1.set_title('Arrows scale with plot width, not view')
Q = ax1.quiver(X2[::3, ::3], Y2[::3, ::3], u2D[::3, ::3], v2D[::3, ::3], units='width')
qk = ax1.quiverkey(Q, 0.9, 0.9, 2, r'$2 \frac{m}{s}$', labelpos='E',
                   coordinates='figure')
plt.show()