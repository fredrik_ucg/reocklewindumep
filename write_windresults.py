import numpy as np
import rasterio
import os

def make_test_data(winddir,u, v, w, lambdaM, x, y, z, buildIndex, buildIndexB, un, vn, wn, nz):
    vplot, uplot, wplot, Len = parse_results(winddir, u, v, w, lambdaM, x, y, z, buildIndex, buildIndexB, un, vn, wn, nz)
    np.savez("tests/baserun5.npz",Len=Len,lambdaM=lambdaM)

def test_results(winddir,u, v, w, lambdaM, x, y, z, buildIndex, buildIndexB, un, vn, wn, nz):
    test_data = np.load("tests/baserun5.npz")
    vplot, uplot, wplot, Len = parse_results(winddir, u, v, w, lambdaM, x, y, z, buildIndex, buildIndexB, un, vn, wn,nz)
    if not np.allclose(Len,test_data['Len']):
        print("error in Len")
        print(Len)
    if not np.allclose(lambdaM,test_data['lambdaM']):
        print("error in lambdaM")
        print(Len)
    print("all tests done!")


def write_results_to_geotif(basename, geotif_profile,result_dict, height, outdir = "."):

    for wd,result in result_dict.items():
        #vplot, uplot, wplot,Len = v
        _, _, _, Len = result

        slice = Len[:,:,height]
        slice = slice.astype(geotif_profile['dtype'])

        with rasterio.open(os.path.join(outdir,f"{basename}_{wd}.tif"), 'w', **geotif_profile) as dst:
            dst.write(slice, 1)

def parse_results(u, v, w,  x, y, z,  buildIndexB, nz):
    nx = x.shape[0]
    ny = y.shape[0]
    uplot = np.zeros([nx, ny, nz])
    vplot = np.zeros([nx, ny, nz])
    wplot = np.zeros([nx, ny, nz])

    vplot[0:nx, :, :] = 0.5 * (v[0:nx, :, :] + v[1:nx + 1, :, :])
    uplot[:, 0:ny, :] = 0.5 * (u[:, 0:ny, :] + u[:, 1:ny + 1, :])
    wplot[:, :, 0:nz] = 0.5 * (w[:, :, 0:nz] + w[:, :, 1:nz + 1])
    Len = np.sqrt(uplot ** 2 + vplot ** 2 + wplot ** 2 + 2.2e-16)

    Len[buildIndexB[0],buildIndexB[1],:]=0

    #reLen = np.reshape(Len, (nx, ny, nz))
    Len = Len[::-1]
    vplot = vplot[::-1]
    uplot = uplot[::-1]
    wplot = wplot[::-1]
    return vplot,uplot,wplot,Len