import numpy as np
#from math import atan2, hypot
from InnerRegions import InnerRegions

def BackZone(j, NN, dd, buildCenter, Dir, Dir2, Dir3, effwid, theta, rad, Pois, altPoints, wallIndex, nx, ny):


    if dd <= 90.:
        orientation = (270 - dd) * rad
    else:
        orientation = (270 - (360 + dd)) * rad


    if len(Pois) <= 1:
        Poi = altPoints[np.where(altPoints[:, Dir] == np.min(altPoints[:, Dir]))]
        Poj = altPoints[np.where(altPoints[:, Dir] == np.max(altPoints[:, Dir]))]
        Pois = np.array(list(Poi) + list(Poj))
        if len(Pois) > 2:
            Pois = np.array([list(t) for t in set(tuple(element) for element in Pois)])
            Pois1 = Pois[np.where(Pois[:, Dir] == np.min(Pois[:, Dir]))]
            Pois2 = Pois[np.where(Pois[:, Dir] == np.max(Pois[:, Dir]))]
            if len(Pois1) > 1:
                if (dd > 315.) or (dd < 135.):
                    Pois1 = Pois1[np.where(Pois1[:, Dir2] == np.min(Pois1[:, Dir2]))]
                else:
                    Pois1 = Pois1[np.where(Pois1[:, Dir2] == np.max(Pois1[:, Dir2]))]
            if len(Pois2) > 1:
                if (dd > 315.) or (dd < 135.):
                    Pois2 = Pois2[np.where(Pois2[:, Dir2] == np.min(Pois2[:, Dir2]))]
                else:
                    Pois2 = Pois2[np.where(Pois2[:, Dir2] == np.max(Pois2[:, Dir2]))]
            Pois = np.array(list(Pois1) + list(Pois2))


    dl = np.array(Pois[0]) - np.array(Pois[1])
    effwidth = np.sqrt((dl * dl).sum())

    NNxindex = []


    NNx = np.trim_zeros(NN[j])

    for i in np.arange(0, len(NNx)):
        if (len(Pois) < 1):
            continue

        else:
            if effwid[j]*0.9 > effwidth * 1.1:
                NNxindex.append(
                    InnerRegions(i, j, nx, ny, np.array(Pois[0]), np.array(Pois[1]), NNx[i], theta, orientation,
                                 effwidth, wallIndex, [np.asarray(buildCenter[j])], dd))
            else:
                NNxindex.append(InnerRegions(i, j, nx, ny, np.array(Pois[0]), np.array(Pois[1]), NNx[i], theta, orientation,
                                         effwid[j]*0.9, wallIndex, [np.asarray(buildCenter[j])], dd))


    return NNxindex, NNx, Pois