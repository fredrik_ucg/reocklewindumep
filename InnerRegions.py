
import numpy as np
from math import atan2
from scipy.spatial import distance
import matplotlib.pyplot as plt

### inner regions ###
from skimage.draw import polygon, polygon_perimeter
from skimage.measure import label, regionprops
from skimage.segmentation import random_walker

def InnerRegions(i, j, nx, ny, POi, POj, NNx, theta, orientation, effWidth, wallInd, Center, dd):

    # This is the function calculating the inside coordinates of the flow regimes, respectively
    # 2017-Oct-19
    # Sandro Oswald, sandro.oswald@boku.ac.at
    # Vienna Urban Climate Group
    # University of natural sciences (BOKU)

    if NNx < 1.:
        NNx = 1.

    if effWidth is not None:
        effWi = effWidth
    else:
        dl = POi - POj
        effWi = np.sqrt((dl * dl).sum())
        if NNx > effWi / 1.5:
            NNx = effWi / 1.5



    centerX = (POi[1] + POj[1]) / 2
    centerY = (POi[0] + POj[0]) / 2
    xxN = (NNx) * np.sin(theta) + centerX
    yyN = (effWi / 2.) * np.cos(theta) + centerY
    xxN2 = (xxN - centerX) * np.cos(orientation) - (yyN - centerY) * np.sin(orientation) + centerX
    yyN2 = (xxN - centerX) * np.sin(orientation) + (yyN - centerY) * np.cos(orientation) + centerY

    xx2 = np.round(xxN2)
    yy2 = np.round(yyN2)

    NNstack = np.column_stack((xx2.astype(int), yy2.astype(int)))


    def calcDir(p0, p1, p2):
        v1x = float(p1[0]) - float(p0[0])
        v1y = float(p1[1]) - float(p0[1])
        v2x = float(p2[0]) - float(p0[0])
        v2y = float(p2[1]) - float(p0[1])
        dir = v1x * v2y - v1y * v2x
        return dir



    if wallInd is not None:

        NNstack = np.flipud(NNstack)
        startP = NNstack[distance.cdist(np.fliplr([POi]), NNstack).argmin()]
        endP = NNstack[distance.cdist(np.fliplr([POj]), NNstack).argmin()]

        startP[startP < 0] = 0
        endP[endP < 0] = 0

        indexA = np.where((NNstack[:, 0] == startP[0]) & (NNstack[:, 1] == startP[1]))
        indexB = np.where((NNstack[:, 0] == endP[0]) & (NNstack[:, 1] == endP[1]))

        if (dd >= 45.) and (dd <= 225.):
            if (indexA[0][0] < indexB[0][-1]) and (calcDir(startP, endP, NNstack[indexA[0][0] + 2]) > 0.0):
                NNstack = NNstack[indexA[0][0]:indexB[0][-1]]

            elif (indexA[0][0] < indexB[0][-1]) and (calcDir(startP, endP, NNstack[indexA[0][0] + 2]) <= 0.0):
                NNstack = np.row_stack((NNstack[0:indexA[0][0]], NNstack[indexB[0][-1]:-1]))

            elif (indexA[0][0] > indexB[0][-1]) and (calcDir(startP, endP,
                                                         NNstack[indexB[0][-1] + 2]) > 0.0):
                NNstack = NNstack[indexB[0][-1]:indexA[0][0]]

            else:
                NNstack = np.row_stack((NNstack[0:indexB[0][-1]], NNstack[indexA[0][0]:-1]))
        else:
            if (indexA[0][0] < indexB[0][-1]) and (calcDir(startP, endP, NNstack[indexA[0][0] + 2]) <= 0.0):
                NNstack = NNstack[indexA[0][0]:indexB[0][-1]]

            elif (indexA[0][0] < indexB[0][-1]) and (calcDir(startP, endP, NNstack[indexA[0][0] + 2]) > 0.0):
                NNstack = np.row_stack((NNstack[0:indexA[0][0]], NNstack[indexB[0][-1]:-1]))

            elif (indexA[0][0] > indexB[0][-1]) and (calcDir(startP, endP,
                                                         NNstack[indexB[0][-1] + 2]) < 0.0):
                NNstack = NNstack[indexB[0][-1]:indexA[0][0]]

            else:
                NNstack = np.row_stack((NNstack[0:indexB[0][-1]], NNstack[indexA[0][0]:-1]))

        NNstack[:, 1][NNstack[:, 1] >= nx] = nx - 1
        NNstack[:, 0][NNstack[:, 0] >= ny] = ny - 1
        NNstack = NNstack[(NNstack[:, 1] >= 0) & (NNstack[:, 1] < (nx))]
        NNstack = NNstack[(NNstack[:, 0] >= 0) & (NNstack[:, 0] < (ny))]

        if len(NNstack) >= 2:

            if (dd > 89.) and (dd < 91.) or (dd > 179.) and (dd < 181.) or (dd > 269.) and (dd < 271.) or (dd > 359.) or (dd < 1.):
                NNstack = np.vstack((NNstack, np.fliplr(np.int_(Center))))

            border_points_x = np.array([p[0] for p in NNstack] + [NNstack[0][0]])
            border_points_y = np.array([p[1] for p in NNstack] + [NNstack[0][1]])

            image = np.zeros((nx + 1, ny + 1))

            filled_rr, filled_cc = polygon(border_points_y, border_points_x)

            image[filled_rr, filled_cc] = 1

            border_rr, border_cc = polygon_perimeter(border_points_y, border_points_x, shape=image.shape, clip=True)

            image[border_rr, border_cc] = 0

            label_img, num_regions = label(image, background=0, return_num=True)

            regions = regionprops(label_img)

            if len(regions) == 0:
                NNindex = np.fliplr(NNstack)
            else:
                inner_region = regions[0]
                NNindex = inner_region.coords
        else:
            NNindex = wallInd

    else:
        NNstack = NNstack[(NNstack[:, 1] >= 0) & (NNstack[:, 1] <= (nx))]
        NNstack = NNstack[(NNstack[:, 0] >= 0) & (NNstack[:, 0] <= (ny))]

        border_points_x = np.array([p[0] for p in NNstack] + [NNstack[0][0]])
        border_points_y = np.array([p[1] for p in NNstack] + [NNstack[0][1]])

        image = np.zeros((nx + 1, ny + 1))

        # polygon() calculates the indices of a filled polygon
        # one would expect this to be inner+border but apparently it is (inner+border)/2
        filled_rr, filled_cc = polygon(border_points_y, border_points_x)
        # set the image to white at these points
        image[filled_rr, filled_cc] = 1
        # print(i)

        if (len(set(border_points_x)) <= 1) and (len(set(border_points_y)) <= 1):
            border_points_y[1] = border_points_y[1] + 1
            border_points_x[1] = border_points_x[1] + 1

        # polygon_perimeter() calculates the indices of a polygon perimiter (i.e. border)
        border_rr, border_cc = polygon_perimeter(border_points_y, border_points_x)
        # exclude border, by setting it to black
        image[border_rr, border_cc] = 0

        # label() detects connected patches of the same color and enumerates them
        label_img, num_regions = label(image, background=0, return_num=True)

        # regionprops() takes a labeled image and computes some measures for each region
        regions = regionprops(label_img)
        if len(regions) == 0:
            NNindex = np.fliplr(NNstack)
        else:
            inner_region = regions[0]
            NNindex = inner_region.coords



    return NNindex
