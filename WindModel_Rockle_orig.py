from __future__ import print_function

import os

import matplotlib.pyplot as plt
import numpy as np
from osgeo import gdal
# from mpl_toolkits.mplot3d import axes3d
# import Solweig_v2015_metdata_noload as metload

import WindModel_Rockle.FlowZones_Rockle as FlowZones


infolder = 'Wind_Gotheborg/'
outputfolder = 'Wind_Gotheborg/Output/'

showimage = 0
usevegdem = 0
onlyglobal = 0

landcover = 1
sensorheight = 26.0

UTC = 0
lon = 11.953 #
lat = 57.706 #
alt = 0

metfile = 1
if metfile == 1:
    met = np.loadtxt(infolder + "2017061920_BOKU_ENGBA.txt",skiprows=1, delimiter=' ')
else:
    met = np.zeros((1, 24)) - 999.

    year = 2016
    month = 6
    day = 21
    hour = 12
    minu = 00

    if (year % 4) == 0:
        if (year % 100) == 0:
            if (year % 400) == 0:
                leapyear = 1
            else:
                leapyear = 0
        else:
            leapyear = 1
    else:
        leapyear = 0

    if leapyear == 1:
        dayspermonth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    else:
        dayspermonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    doy = np.sum(dayspermonth[0:month - 1]) + day

    Ta = 25.
    RH = 50
    radG = 880.
    radD = 150.
    radI = 950.

    met[0, 0] = year
    met[0, 1] = doy
    met[0, 2] = hour
    met[0, 3] = minu
    met[0, 11] = Ta
    met[0, 10] = RH
    met[0, 14] = radG
    met[0, 21] = radD
    met[0, 22] = radI

location = {'longitude': lon, 'latitude': lat, 'altitude': alt}
# YYYY, altitude, azimuth, zen, jday, leafon, dectime, hour, altmax = metload.Solweig_2015a_metdata_noload(met, location, UTC)


dataSet = gdal.Open(infolder + "LC_Goe.tif")
lc_grid = dataSet.ReadAsArray().astype(np.float)
sitein = "landcoverclasses_2016a.txt"
f = open(sitein)
lin = f.readlines()
lc_class = np.zeros((lin.__len__() - 1, 6))
for i in range(1, lin.__len__()):
    lines = lin[i].split()
    for j in np.arange(1, 7):
        lc_class[i - 1, j - 1] = float(lines[j])

lc_gridR = np.copy(lc_grid[::-1])

buildings = np.copy(lc_gridR)
buildings[buildings == 7] = 1
buildings[buildings == 6] = 1
buildings[buildings == 5] = 1
buildings[buildings == 4] = 1
buildings[buildings == 3] = 1
buildings[buildings == 2] = 0

rows = buildings.shape[0]
cols = buildings.shape[1]

buildings[0, :] = 1.
buildings[:, 0] = 1.
buildings[-1, :] = 1.
buildings[:, -1] = 1.

dataSet = gdal.Open(infolder + "WallAspect_Goe.tif")
dirwalls = dataSet.ReadAsArray().astype(np.float)
dirwalls[dirwalls < 0.] = 0.
dirwalls = dirwalls[::-1]

dataSet = gdal.Open(infolder + "WallHeight_Goe.tif")
walls = dataSet.ReadAsArray().astype(np.float)
walls[walls < 0.] = 0.
walls = walls[::-1]

# #dataSet.RasterXSize
dataSet.GetGeoTransform()
geotransform = dataSet.GetGeoTransform()
scaleP = 1 / geotransform[1]

voxelheight = geotransform[1]

DOY = met[:, 1]
hours = met[:, 2]
minu = met[:,3]
Ws = met[:, 9]
Wd = met[:, 23]
##############

if rows < cols:
    nx = cols  # Number of steps in space(x)
    ny = cols  # Number of steps in space(y)
elif rows == cols:
    nx = rows  # Number of steps in space(x)
    ny = cols  # Number of steps in space(y)
else:
    nx = rows  # Number of steps in space(x)
    ny = rows  # Number of steps in space(y)



def WindModel_Rockle(i, nx, ny, Ws, Wd, zref, builds, dirwalls, lc_gridR, voxelheight):

    # This is the main function calculating the wind scheme model
    # 2017-Sept-05
    # Sandro Oswald, sandro.oswald@boku.ac.at
    # Vienna Urban Climate Group
    # University of natural sciences (BOKU)

    ff = Ws[i]
    dd = Wd[i]
    print(ff,dd,'Wind')
    if dd == 360.:
        dd = 0.

    rad = np.pi/180.

    L = nx / 1.
    B = ny / 1.

    dx = voxelheight  # Width of space step(x)
    dy = voxelheight  # Width of space step(y)

    buildIndex, buildGrid3D, un, vn, wn, u, v, w, nz = FlowZones.FlowZonesR(builds, dirwalls, walls, lc_gridR, ff, dd, zref, nx, ny,
                                                              dx, dy, rad)


    H = nz / 1.
    dz = H / (nz)  # Width of space step(z)

    x = np.linspace(0, L, nx)  # Range of x(0,L) and specifying grid points
    y = np.linspace(0, B, ny)  # Range of y(0,H) and specifying grid points
    z = np.linspace(0, H, nz)

    lambdaM = np.zeros([nx, ny, nz])  # Preallocating lambda
    lambdaM1 = np.zeros([nx, ny, nz])  # Preallocating lambda + 1


    lambdaM[0, :, :] = 0.
    lambdaM[:, 0, :] = 0.
    lambdaM[:, :, 0] = 0.
    lambdaM[-1, :, :] = 0.
    lambdaM[:, -1, :] = 0.
    lambdaM[:, :, -1] = 0.
    lambdaM1[0, :, :] = 0.
    lambdaM1[:, 0, :] = 0.
    lambdaM1[:, :, 0] = 0.
    lambdaM1[-1, :, :] = 0.
    lambdaM1[:, -1, :] = 0.
    lambdaM1[:, :, -1] = 0.

    Xi = ((np.cos(np.pi / nx) + (dx / dy) ** 2 * np.cos(np.pi / ny)) / (1 + (dx / dy) ** 2)) ** 2

    omega = 2. * ((1 - np.sqrt(1 - Xi)) / Xi)
    if (omega < 1) or (omega > 2):
        omega = 1.78

    iteration = 15
    index = 0
    indices = np.transpose(np.where(buildGrid3D == 1.))
    indices = indices[indices[:, 0] > 0]
    indices = indices[indices[:, 1] > 0]
    indices = indices[indices[:, 2] > 0]
    indices = indices[indices[:, 0] < nx-1]
    indices = indices[indices[:, 1] < ny-1]
    indices = indices[indices[:, 2] < nz-1]

    alphaH = 1.
    alphaV = 3.
    alpha = alphaH / alphaV

    e = np.ones([nx, ny, nz])
    f = np.ones([nx, ny, nz])
    g = np.ones([nx, ny, nz])
    h = np.ones([nx, ny, nz])
    m = np.ones([nx, ny, nz])
    n = np.ones([nx, ny, nz])
    o = np.ones([nx, ny, nz])
    p = np.ones([nx, ny, nz])
    q = np.ones([nx, ny, nz])

    buildIndexB = np.stack(buildIndex)

    e[buildIndexB[0], buildIndexB[1] - 1, buildIndexB[2]] = 0.
    f[buildIndexB[0], buildIndexB[1] + 1, buildIndexB[2]] = 0.
    g[buildIndexB[0] - 1, buildIndexB[1], buildIndexB[2]] = 0.
    h[buildIndexB[0] + 1, buildIndexB[1], buildIndexB[2]] = 0.
    #m[buildIndexB[0], buildIndexB[1], buildIndexB[2] - 1] = 0.
    n[buildIndexB[0], buildIndexB[1], buildIndexB[2] + 1] = 0.

    o[buildIndexB[0], buildIndexB[1] - 1, buildIndexB[2]] = 0.5
    o[buildIndexB[0], buildIndexB[1] + 1, buildIndexB[2]] = 0.5
    p[buildIndexB[0] - 1, buildIndexB[1], buildIndexB[2]] = 0.5
    p[buildIndexB[0] + 1, buildIndexB[1], buildIndexB[2]] = 0.5
    #q[buildIndexB[0], buildIndexB[1], buildIndexB[2] - 2] = 0.5
    q[buildIndexB[0], buildIndexB[1], buildIndexB[2] + 1] = 0.5

    Aj = dx ** 2 / dy ** 2
    Bk = alpha**2 * dx ** 2 / dz ** 2

    while index <= iteration:

        lambdaM = np.copy(lambdaM1)
        #############
        for i, j, k in indices:
            lambdaM1[i, j, k] = omega * (
            ((-1.) * (dx ** 2 * (-2. * alphaH ** 2) * (((un[i, j+1, k] - un[i, j, k]) / (dx)  + (
                vn[i+1, j, k] - vn[i, j, k]) / (dy) +
                (wn[i, j, k+1] - wn[i, j, k]) / (dz)))) + (
                 e[i, j, k] * lambdaM[i, j + 1, k] + f[i, j, k] * lambdaM1[i, j - 1, k] + Aj * (
                     g[i, j, k] * lambdaM[i + 1, j, k] + h[i, j, k] * lambdaM1[i - 1, j, k]) + Bk * (
                     m[i, j, k] * lambdaM[i, j, k + 1] + n[i, j, k] * lambdaM1[i, j, k - 1]))) / (
                2. * (o[i, j, k] + Aj * p[i, j, k] + Bk * q[i, j, k]))) + (1 - omega) * lambdaM[i, j, k]


        print(np.sum(np.abs(lambdaM1 - lambdaM))/np.sum(np.abs(lambdaM1)))
        if np.sum(np.abs(lambdaM1 - lambdaM))/np.sum(np.abs(lambdaM1)) < 1e-5:
            break
        else:
            lambdaM[0, :, :] = 0.
            lambdaM[:, 0, :] = 0.
            lambdaM[:, :, 0] = 0.
            lambdaM[-1, :, :] = 0.
            lambdaM[:, -1, :] = 0.
            lambdaM[:, :, -1] = 0.
            lambdaM1[0, :, :] = 0.
            lambdaM1[:, 0, :] = 0.
            lambdaM1[:, :, 0] = 0.
            lambdaM1[-1, :, :] = 0.
            lambdaM1[:, -1, :] = 0.
            lambdaM1[:, :, -1] = 0.

            u[:, 1:ny, :] = un[:, 1:ny, :] + 0.5 * (
                        1. / (alphaH ** 2)) * (lambdaM1[:, 1:ny, :] - lambdaM1[:, 0:ny-1, :]) / dy
            v[1:nx, :, :] = vn[1:nx, :, :] + 0.5 * (
                        1. / (alphaH ** 2)) * (lambdaM1[1:nx, :, :] - lambdaM1[0:nx-1, :, :]) / dx
            w[:, :, 1:nz] = wn[:, :, 1:nz] + 0.5 * (
                        1. / (alphaV ** 2)) * (lambdaM1[:, :, 1:nz] - lambdaM1[:, :, 0:nz-1]) / dz



        print("Iteration",index," out of ",iteration)
        index = index + 1

    return u, v, w, lambdaM1, x, y, z, buildIndex, buildIndexB, un, vn, wn, nz


# Loop through time series
for i in np.arange(0, 1):
    u, v, w, lambdaM, x, y, z, buildIndex, buildIndexB, un, vn, wn, nz = WindModel_Rockle(i, nx, ny, Ws, Wd, sensorheight,
                                                                         buildings, dirwalls, lc_gridR, voxelheight)

    u[buildIndex] = 0.
    v[buildIndex] = 0.
    w[buildIndex] = 0.
    uplot = np.zeros([nx, ny, nz])
    vplot = np.zeros([nx, ny, nz])
    wplot = np.zeros([nx, ny, nz])

    vplot[0:nx, :, :] = 0.5 * (v[0:nx, :, :] + v[1:nx + 1, :, :])
    uplot[:, 0:ny, :] = 0.5 * (u[:, 0:ny, :] + u[:, 1:ny + 1, :])
    wplot[:, :, 0:nz] = 0.5 * (w[:, :, 0:nz] + w[:, :, 1:nz + 1])
    Len = np.sqrt(uplot ** 2 + vplot ** 2 + wplot ** 2 + 2.2e-16)

    reLen = np.reshape(Len, (nx, ny, nz))
    # if (hours[i]) < 10:
    #     filename = 'Windspeed_Goe_' + str(int(YYYY[0, i])) + '_' + str(int(DOY[i])) + '_0' + str(
    #         int(hours[i])) + str(int(minu[i])) + '.txt'
    #     filename = outputfolder + filename
    # else:
    #     filename = 'Windspeed_Goe_' + str(int(YYYY[0, i])) + '_' + str(int(DOY[i])) + '_' + str(
    #         int(hours[i])) + str(int(minu[i])) + '.txt'
    #     filename = outputfolder + filename

    filename = os.path.join(outputfolder,"Windspeed_Goe_%s.txt" % (Wd[i]))

    with open(filename, 'w') as outfile:
        for slice_2d in reLen:
            np.savetxt(outfile, slice_2d, fmt='%10.2f', delimiter='\t')

    print(reLen.shape)
    #newData = np.loadtxt('test.txt').reshape((nx,ny,nz))
    #assert np.all(newData == Len)

u2D = uplot[:, :, 2]
v2D = vplot[:, :, 2]
w2D = vplot[50, :, :]
Len2D = Len[:, :, 2]

uplot = np.divide(uplot, Len)
vplot = np.divide(vplot, Len)
wplot = np.divide(wplot, Len)


X3, Y3, Z3 = np.meshgrid(x, y, z)
X2, Y2 = np.meshgrid(x, y)

##############################
## Plots
# Create a mask
mask = np.zeros(Len2D.shape, dtype=bool)
mask[buildIndexB[0], buildIndexB[1]] = True
#u2D = np.ma.array(u2D, mask=mask)
#v2D = np.ma.array(v2D, mask=mask)

fig, ax2 = plt.subplots()
lw = 3. * Len2D / Len2D.max()
strm = ax2.streamplot(X2, Y2, u2D, v2D, color=Len2D, density=10.0, linewidth=0.8, cmap=plt.cm.autumn) #density=5.6, color='r', linewidth=lw)
#ax2.set_title('Varying Line Width')
fig.colorbar(strm.lines)
ax2.imshow(~mask, alpha=0.5, origin="lower",
          interpolation='spline16',
           cmap='gray', aspect='auto')
#ax2.set_aspect('equal')
#plt.gca().invert_yaxis()
plt.show()